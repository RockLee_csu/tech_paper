### Technical Paper

#### Problem: N Body problem
#### Advisor: Shantenu Jha

*****
### 1.Serial:
files:    
**body.py**: body class (in python)    
**sequential_graph.py**: serial code (using pygame to draw graph)    
**sequential_comp.py**: serial code (only use is for computation)      
*****
How to test it:    
**run**: 
```
python sequential_comp.py
```
*****

*****
### 2.threading:
files:    
**body.py**: body class (in python)       
**thread_comp_2.py**: multithread code      
*****
How to test it:    
**run**: 
```
python process_comp_2.py
```
*****

*****
### 3.multiprocessing:
files:    
**body.py**: body class (in python)       
**process_comp.py**: multithread code      
*****
How to test it:    
**run**: 
```
python thread_comp.py
```
*****

*****
### 4.OpenMP:
files:    
**body.h**: body class (in c++ version)    
**openmp_comp.cpp**: openmp version code    
*****
How to test it:    
**compile**:
``` 
g++ -std=c++11 openmp_comp.cpp -fopenmp
```
**run**: 
```
./a.out
```
*****

*****
### 5.MPI:  
files:    
**body.py**: body class (in python)        
**mpi_comp.py**: MPI version code      
*****
How to test it:   
**run**:    
```
mpiexec -n 4 python mpi_comp.py
```
**4** represent the number of processes   
*****
