#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <omp.h>
#include <ctime>
#include "body.h"

using namespace std;

void store_file(string file_name);
void print_status();
vector<Body> body_list;
const double QUANTUM = 2500.0;

int main(int argc, char** argv) {
	store_file("test_files/in_10000.txt");
	unsigned iteration = 0;
	bool running = true;
	omp_set_num_threads(8);
	
	double t1 = omp_get_wtime();
	while (running) {
		if (iteration == 3) {
			break;
		}
		++iteration;
		int i, j;
		// 2-d for loop: calculate the force
		#pragma omp parallel shared(body_list, i) private(j)
		{
			#pragma omp for schedule(static)
				for (i = 0; i < body_list.size(); ++i) {
					body_list[i].reset_force();
			
	//				#pragma omp for schedule(dynamic,1)
						for (j = 0; j < body_list.size(); ++j) {
							if (i != j) {
								body_list[i].add_force(body_list[j]);
							}
						}
				}
			// 1-d loop: using the force to update the position of N body
			#pragma omp for schedule(static)
			for (i = 0; i < body_list.size(); ++i) {
				body_list[i].update(QUANTUM);
			}
		}
	}
	double t2 = omp_get_wtime();
	double wtick = omp_get_wtick();
	float diff = t2 - t1;
//	cout << "iteration " << iteration << endl;
	cout << "time: " << diff / wtick << endl;

//	print_status();
	return 0;
}

void store_file(string file_name) {
	ifstream infile(file_name);
	string line;
	if (infile.is_open()) {
		while (getline(infile, line)) {
			stringstream ss(line);
			string token;
			vector<double> tmp;
			while (ss >> token) {
				tmp.push_back(stod(token));
			}
			Body particle(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4]);
			body_list.push_back(particle);
		}
		infile.close();
	}
	else {
		cout << "Unable to find the file" << endl;
	}
}

void print_status() {
	for (int i = 0; i < body_list.size(); ++i) {
		cout << body_list[i].p_x  << " " << body_list[i].p_y << endl;
	}
}