#include <cmath>

class Body {
public:
	double p_x;
	double p_y;
	double m;
	double v_x;
	double v_y;
	double f_x;
	double f_y;

	static double e;
	static double G;
public:
	// Constructor
	Body(double pos_x, double pos_y, double velocity_x, double velocity_y, double mass) :p_x(pos_x), p_y(pos_y), m(mass), v_x(velocity_x), v_y(velocity_y) {}

	// set the force to 0 for the next iteration
	void reset_force() {
		f_x = 0;
		f_y = 0;
	}

	//Compute the net force acting between the body self and body particle, and add net force to body self
	void add_force(Body particle);

	// update the velocity and position using a timestep dt
	void update(double dt);
	friend void print_status();

	// helper function()
	double get_pos_x() {
		return p_x;
	}

	double get_pos_y() {
		return p_y;
	}
};

double Body::e = 3 * pow(10, -4);
double Body::G = 6.67 * pow(10, -11);

void Body::add_force(Body particle) {
	double dx = particle.p_x - p_x;
	double dy = particle.p_y - p_y;
	double dist = sqrt(dx * dx + dy * dy);
	double F = (Body::G * this->m * particle.m) / (dist * dist + Body::e);
	f_x += F * dx / sqrt(dist * dist + Body::e);
	f_y += F * dy / sqrt(dist * dist + Body::e);
}

void Body::update(double dt) {
	v_x += dt * f_x / m;
	v_y += dt * f_y / m;
	p_x += v_x * dt;
	p_y += v_y * dt;
}