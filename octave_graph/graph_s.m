n = [101 300 500 1000 3000 6000 10000 20000]; % size: 8
serial = [0.0174 0.1544 0.425 1.69 15.21 63.56 191.23 777];

subplot(2, 1, 1), plot(n, serial, 'r', 'LineWidth', 2), ti = title('Sequential Version'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('serial code'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);
subplot(2, 1, 2), semilogx(n, serial, 'r', 'LineWidth', 2), ti = title('Sequential Version (Semilogarithmic)'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('serial code'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);