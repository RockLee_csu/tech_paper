n = [101 300 500 1000 3000 6000 10000 20000]; % size: 8
thread2 = [0.0256 0.223 0.604 2.31 20.91 87.94 258.57 1043.84];
thread4 = [0.0306 0.231 0.632 2.54 23.75 97.10 278.07 1112.28];
thread8 = [0.0378 0.231 0.658 2.54 23.2 99.16 283.86 1135.44];

subplot(2, 1, 1), plot(n, thread2, 'r', 'LineWidth', 2, n, thread4, 'b', 'LineWidth', 2, n, thread8, 'g', 'LineWidth', 2), ti = title('threading Version'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('2 threads', '4 threads', '8 threads'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);
subplot(2, 1, 2), semilogx(n, thread2, 'r', 'LineWidth', 2, n, thread4, 'b', 'LineWidth', 2, n, thread8, 'g', 'LineWidth', 2), ti = title('threading Version (Semilogarithmic)'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('2 threads', '4 threads', '8 threads'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);