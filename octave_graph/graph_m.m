n = [101 300 500 1000 3000 6000 10000 20000]; % size: 8
mpi = [0.0337 0.163 0.296 0.662 6.98 19.169 68.63 218.64];

subplot(2, 1, 1), plot(n, mpi, 'r', 'LineWidth', 2), ti = title('MPI Version'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('mpi code'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);
subplot(2, 1, 2), semilogx(n, mpi, 'r', 'LineWidth', 2), ti = title('MPI Version (Semilogarithmic)'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('mpi code'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);