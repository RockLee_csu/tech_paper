n = [101 300 500 1000 3000 6000 10000 20000]; % size: 8
process2 = [0.0147 0.088 0.232 0.918 11.27 37.46 114.93 459.72];
process4 = [0.0172 0.087 0.231 0.530 5.625 21.68 73.37 230.4];
process8 = [0.0241 0.096 0.174 0.401 4.11 13.04 43.71 143.84]

subplot(2, 1, 1), plot(n, process2, 'r', 'LineWidth', 2, n, process4, 'b', 'LineWidth', 2, n, process8, 'g', 'LineWidth', 2), ti = title('multiprocessing Version'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('2 processes', '4 processes', '8 processes'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);
subplot(2, 1, 2), semilogx(n, process2, 'r', 'LineWidth', 2, n, process4, 'b', 'LineWidth', 2, n, process8, 'g', 'LineWidth', 2), ti = title('multiprocessing Version (Semilogarithmic)'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('2 processes', '4 processes', '8 processes'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);