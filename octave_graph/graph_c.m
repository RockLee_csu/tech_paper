n = [101 300 500 1000 3000 6000 10000 20000]; % size: 8
serial = [0.0174 0.1544 0.425 1.69 15.21 63.56 191.23 777];
thread = [0.0256 0.223 0.604 2.31 20.91 87.94 258.57 1043.84];
process = [0.0241 0.096 0.174 0.401 4.11 13.04 43.71 143.84];
mpi = [0.0337 0.163 0.296 0.662 6.98 19.169 68.63 218.64];

subplot(2, 1, 1), plot(n, serial, 'r', 'LineWidth', 2, n, thread, 'b', 'LineWidth', 2, n, process, 'g', 'LineWidth', 2, n, mpi, 'm', 'LineWidth', 2), ti = title('Comparison'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('serial', '2 threads', '8 processes', '8 processes mpi'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);

subplot(2, 1, 2), semilogx(n, serial, 'r', 'LineWidth', 2, n, thread, 'b', 'LineWidth', 2, n, process, 'g', 'LineWidth', 2, n, mpi, 'm', 'LineWidth', 2), ti = title('Comparison (Semilogarithmic)'), hx = xlabel('Number of particles'), hy = ylabel('Time (sec)'), legend('serial', '2 threads', '8 processes', '8 processes mpi'), set(gca, 'FontSize', 15), set(hx, 'FontSize', 15), set(hy, 'FontSize', 15), set(ti, 'FontSize', 15);