##################################
#  threading version code        # 
#  Just for computation 	     #
#  Author: Yimeng Li             #
##################################

from body import *
import threading
import time

###################################### store the file into a list: lines
with open("test_files/in_10000.txt", "r") as fhand:
	lines = fhand.read().splitlines()
fhand.close()			# close the file
######################################

###################################### Define some constants
QUANTUM = 2500.0			# the time quantum
######################################

###################################### Number of threads
NUM_THREAD = 8
threads = []
######################################

def worker_force(s_i, e_i):
	"""calculate the force of the particles from index s_i to e_i(not included)"""
	global body_list
	for i in xrange(s_i, e_i):
		body_list[i].reset_force()
		for j in xrange(len(body_list)):
			if j != i:
				body_list[i].add_force(body_list[j])

def worker_update(s_i, e_i):
	"""update the location of particles from s_i to e_i(not included)"""
	global body_list
	for i in xrange(s_i, e_i):
		body_list[i].update(QUANTUM)

body_list = []
for s in lines:
	attr = s.split()
	particle = Body(float(attr[0]), float(attr[1]), float(attr[2]), 
		float(attr[3]), float(attr[4]))
	body_list.append(particle)

###################################### Try to split the job into several threads equally
QUA = len(body_list) / NUM_THREAD
st = []
ed = []
tmp_var = 0
for i in xrange(NUM_THREAD):
	st.append(tmp_var)
	if i == NUM_THREAD - 1:
		ed.append(len(body_list))
	else:
		ed.append(tmp_var + QUA)
		tmp_var += QUA
######################################
iteration = 0
running = True
start_time = time.time()
while running:
	if iteration == 4:
		break
	iteration += 1
	for i in xrange(NUM_THREAD):
		t = threading.Thread(target = worker_force, args = (st[i], ed[i], ))
		threads.append(t)
		t.start()

	for t in threads:
		t.join()

	for i in xrange(NUM_THREAD):
		t = threading.Thread(target = worker_update, args = (st[i], ed[i], ))
		threads.append(t)
		t.start()

	for t in threads:
		t.join()

end_time = time.time()
print("--- %s seconds ---" % ((end_time - start_time) / iteration))