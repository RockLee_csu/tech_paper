##################################
#  MPI code                      # 
#  Just for computation  	     #
#  Author: Yimeng Li             #
##################################

from mpi4py import MPI
from body import *

# set up MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
	#####################################
	# store the file into a list: lines
	with open("test_files/in_5.txt", "r") as fhand:
		lines = fhand.read().splitlines()
	fhand.close()
	#####################################

###################################### Define some constants(global variable)
QUANTUM = 2500.0			# the time quantum
CUTOFF = 0.01				# check if two body is the same one (very important in mpi program)
######################################

if rank == 0:
	body_list = []
	chunks = [[] for _ in range(size)]

	for s in lines:
		attr = s.split()
		particle = Body(float(attr[0]), float(attr[1]), float(attr[2]), float(attr[3]), float(attr[4]))
		body_list.append(particle)

	for i, chunk in enumerate(body_list):
		# chunks[i%size] send to rank (i%size) process
		chunks[i%size].append(chunk)
else:
	body_list = None
	chunks = None

# body_list is local, chunks is global data
body_list = comm.scatter(chunks, root = 0)
chunks = comm.allgather(body_list)

running = True
iteration = 0

if rank == 0:
	for i in xrange(len(chunks)):
		for j in xrange(len(chunks[i])):
			print chunks[i][j].pos_x, chunks[i][j].pos_y
	print "start!"
while running:
	if iteration == 10:
		break
	iteration += 1

	for i in xrange(len(body_list)):
		body_list[i].reset_force()
		for j in xrange(len(chunks)):
			for k in xrange(len(chunks[j])):
				dx = body_list[i].pos_x - chunks[j][k].pos_x
				dy = body_list[i].pos_y - chunks[j][k].pos_y
				r2 = dx * dx + dy * dy
				if r2 < CUTOFF * CUTOFF:
					# same body
					continue
				body_list[i].add_force(chunks[j][k])
#	chunks = comm.allgather(body_list)
	for body in body_list:
		body.update(QUANTUM)

	chunks = comm.allgather(body_list)

if rank == 0:
	for i in xrange(len(chunks)):
		for j in xrange(len(chunks[i])):
			print chunks[i][j].pos_x, chunks[i][j].pos_y