##################################
#  sequential version code       # 
#  Just for computation 	     #
#  Author: Yimeng Li             #
##################################

from body import *
import time

###################################### store the file into a list: lines
with open("test_files/in_20000.txt", "r") as fhand:
	lines = fhand.read().splitlines()
fhand.close()			# close the file
######################################

###################################### Define some constants
QUANTUM = 2500.0			# the time quantum
######################################

body_list = []
for s in lines:
	attr = s.split()
	particle = Body(float(attr[0]), float(attr[1]), float(attr[2]), 
		float(attr[3]), float(attr[4]))
	body_list.append(particle)

running = True
iteration = 0
start_time = time.time()
while running:
	if iteration == 4:
		break
	iteration += 1
	# 2-d for-loop: calculate the force

	for i in xrange(len(body_list)):
#		if i % 250 == 0:
#			print str(i), "lala"
		body_list[i].reset_force()
		for j in xrange(len(body_list)):
			if i != j:
				body_list[i].add_force(body_list[j])

	# print the position
	'''
	for i in range(len(body_list)):
		print "body " + str(i), "(" + str("{:.8f}".format(body_list[i].pos_x)), \
			 str("{:.8f}".format(body_list[i].pos_y)) + ")",
	print
	'''

	# 1-d loop: using the force to update the position of N body
	for body in body_list:
		body.update(QUANTUM)

end_time = time.time()
print("--- %s seconds ---" % ((end_time - start_time) / iteration))