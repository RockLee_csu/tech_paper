##################################
#  multiprocesses version_2 code #
#  Using Array  			     # 
#  Just for computation  	     #
#  Author: Yimeng Li             #
##################################

from body import *
from multiprocessing import Process, Array
import time

###################################### store the file into a list: lines
with open("test_files/in_10000.txt", "r") as fhand:
	lines = fhand.read().splitlines()
fhand.close()			# close the file
######################################

###################################### Define some constants
QUANTUM = 2500.0			# the time quantum
######################################

###################################### Number of process
NUM_PROC = 8
process = []
######################################

###################################### UPDATE force
def worker_force(arr_fx, arr_fy, s_i, e_i):
	"""calculate the force of the particles from index s_i to e_i(not included)"""
	for i in xrange(s_i, e_i):
		body_list[i].reset_force()
		for j in xrange(len(body_list)):
			if j != i:
				body_list[i].add_force(body_list[j])
		arr_fx[i] = body_list[i].f_x
		arr_fy[i] = body_list[i].f_y
######################################

###################################### Initilize the list of the Body object
body_list = []
for s in lines:
	attr = s.split()
	particle = Body(float(attr[0]), float(attr[1]), float(attr[2]), 
		float(attr[3]), float(attr[4]))
	body_list.append(particle)
######################################

###################################### Try to split the job into several process equally
QUA = len(body_list) / NUM_PROC
st = []
ed = []
tmp_var = 0
for i in xrange(NUM_PROC):
	st.append(tmp_var)
	if i == NUM_PROC - 1:
		ed.append(len(body_list))
	else:
		ed.append(tmp_var + QUA)
		tmp_var += QUA
######################################

# two arrays (shared memory in multi-processes)
arr_fx = Array('d', range(len(body_list)))
arr_fy = Array('d', range(len(body_list)))

running = True
iteration = 0
start_time = time.time()
while running:
	if iteration == 8:
		break
	iteration += 1
	for i in xrange(NUM_PROC):
		p = Process(target = worker_force, args = (arr_fx, arr_fy, st[i], ed[i], ))
		process.append(p)
		p.start()

	# main process waiting for the others
	for p in process:
		p.join()			
	
	for i in xrange(len(body_list)):
		body_list[i].f_x = arr_fx[i]
		body_list[i].f_y = arr_fy[i]
	
	for body in body_list:
		body.update(QUANTUM)

end_time = time.time()
print("--- %s seconds ---" % ((end_time - start_time) / iteration))