###################################
#  Body class code                #
#  Shared by all versions of code # 
#  Author: Yimeng Li              #
###################################

from math import sqrt
from pygame import draw

class Body(object):
	"""body(particle) class"""

	e = 3 * pow(10, -4)
	G = 6.67 * pow(10, -11)

	def __init__(self, pos_x, pos_y, v_x, v_y, mass):
		"""constructor: create and initialize a new body"""
		
		self.pos_x = pos_x
		self.pos_y = pos_y
		self.mass = mass
		self.v_x = v_x
		self.v_y = v_y
		self.f_x = 0
		self.f_y = 0

		# for display
		self.sz = 4
		self.color = (0, 0, 255)

	def reset_force(self):
		"""set the force to 0 for the next iteration"""
		self.f_x = 0
		self.f_y = 0

	def add_force(self, particle):
		"""Compute the net force acting between the body self and body particle,
		and add net force to body self"""
		dx = particle.pos_x - self.pos_x
		dy = particle.pos_y - self.pos_y
		dist = sqrt(dx * dx + dy * dy)
		F = (Body.G * self.mass * particle.mass) / (dist * dist + Body.e)
		self.f_x += F * dx / sqrt(dist * dist + Body.e)
		self.f_y += F * dy / sqrt(dist * dist + Body.e)

	def update(self, dt):
		"""update the velocity and position using a timestep dt"""
		self.v_x += dt * self.f_x / self.mass;
		self.v_y += dt * self.f_y / self.mass;
		self.pos_x += self.v_x * dt;
		self.pos_y += self.v_y * dt;