##################################
#  sequential version code       # 
#  pygame show the movement	     #
#  Author: Yimeng Li             #
##################################

from body import *
import pygame
from pygame.locals import *

###################################### store the file into a list: lines
with open("test_files/in_5.txt", "r") as fhand:
	lines = fhand.read().splitlines()
#	print len(lines)
fhand.close()			# close the file
######################################

###################################### Define some constants
LEN = 350					# radius of the window
UNIVERSE = 2.50e+11			# the radius of the Universe
QUANTUM = 2500.0			# the time quantum
RATIO = UNIVERSE / LEN		# the ratio between the universe's radio and the resultion
######################################

###################################### for display
background_color = (255, 255, 255)
screen = pygame.display.set_mode((LEN * 2, LEN * 2))
pygame.display.set_caption('N Body Simulation - Sequential Version')
######################################

body_list = []
for s in lines:
	attr = s.split()
	particle = Body(float(attr[0]), float(attr[1]), float(attr[2]), 
		float(attr[3]), float(attr[4]))
	body_list.append(particle)

running = True
it = 0
while running:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False
	screen.fill(background_color)

	# 2-d for-loop: calculate the force
	for i in range(len(body_list)):
		body_list[i].reset_force()
		for j in range(len(body_list)):
			if i != j:
				body_list[i].add_force(body_list[j])

	# print the position
	'''
	for i in range(len(body_list)):
		print "body " + str(i), "(" + str("{:.8f}".format(body_list[i].pos_x)), \
			 str("{:.8f}".format(body_list[i].pos_y)) + ")",
	print
	'''

	# 1-d loop: using the force to update the position of N body
	for body in body_list:
		body.update(QUANTUM)
		draw.circle(screen, body.color, (LEN + int(body.pos_x / RATIO), LEN + int(body.pos_y / RATIO)), int(body.sz), 2)
	pygame.display.flip()		# In order to display the window, using the flip() function